#include "qnvme.h"
#include "ui_qnvme.h"
#include "nvme.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>

#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <sys/mman.h>
#include <sys/user.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/major.h>

#include <QFileDialog>
#include <QMessageBox>
#include <QDateTime>

QNVMe::QNVMe(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::QNVMe)
{
    QPalette p;

    qsrand(0);
    tmpBuf    = (char*)malloc(8 * SIZE_1M);
    tmpRvsBuf = (char*)malloc(8 * SIZE_1M);

    memset(dsm_range, 0x0, sizeof(dsm_range));
    dsm_curr_range = 0;

    InitAdminOpTbl();
    InitNVMeOpTbl();

    buflog.buf = NULL;
    buflog.len = 0;

    ui->setupUi(this);

    ui->te_dmesg->setTextColor(QColor(255,255,255));
    ui->te_dmesg->setStyleSheet("background-color: black");

    //p = ui->te_dmesg->palette();
    //p.setColor(QPalette::Base, QColor(0,0,0));
    //ui->te_dmesg->setPalette(p);

    InitTableWidgets();

    DeviceDiscovery();

    connect(&externalProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(onReadDmesgStdOut()));
    connect(&externalProcess, SIGNAL(readyReadStandardError()), this, SLOT(onReadDmesgStdErr()));

    connect(ui->actionQuit, &QAction::triggered, this, &QWidget::close);

    ui->tab_main->setCurrentIndex(1);
    ui->tab_main->setCurrentIndex(0);

    externalProcess.start("dmesg -wH");

    ui->sb_error_index->setMaximum(id_ctrl.elpe);

    this->setWindowTitle("QNVMe " + QString::fromLatin1(QNVME_VER));
}

QNVMe::~QNVMe()
{
    externalProcess.close();
    free(tmpBuf);
    free(tmpRvsBuf);
    ::close(fd);
    delete ui;
}

void QNVMe::ShowRegisters(char* base)
{
    int pci_fd;
    char path[512];
    void *membase;

    sprintf(path, "/sys/class/nvme/%s/device/resource0", base);
    pci_fd = open(path, O_RDONLY);
    if (pci_fd < 0) {
        sprintf(path, "/sys/class/misc/%s/device/resource0", base);
        pci_fd = open(path, O_RDONLY);
    }

    if (pci_fd < 0) {
        fprintf(stderr, "%s did not find a pci resource\n", base);
        ui->statusBar->showMessage("Please update your kernel to 4.0 or later");
    }
    else
    {
        membase = mmap(0, getpagesize(), PROT_READ, MAP_SHARED, pci_fd, 0);

        if (membase)
        {
            memcpy(&nvme_bar_info, membase, sizeof(struct nvme_bar));

            regs_tblItem[0][1].setText("0x" + QString::number(nvme_bar_info.cap, 16));
            regs_tblItem[1][1].setText("0x" + QString::number(nvme_bar_info.vs, 16));
            regs_tblItem[2][1].setText("0x" + QString::number(nvme_bar_info.intms, 16));
            regs_tblItem[3][1].setText("0x" + QString::number(nvme_bar_info.intmc, 16));
            regs_tblItem[4][1].setText("0x" + QString::number(nvme_bar_info.cc, 16));
            regs_tblItem[5][1].setText("0x" + QString::number(nvme_bar_info.csts, 16));
            regs_tblItem[6][1].setText("0x" + QString::number(nvme_bar_info.nssr, 16));
            regs_tblItem[7][1].setText("0x" + QString::number(nvme_bar_info.aqa, 16));
            regs_tblItem[8][1].setText("0x" + QString::number(nvme_bar_info.asq, 16));
            regs_tblItem[9][1].setText("0x" + QString::number(nvme_bar_info.acq, 16));
            regs_tblItem[10][1].setText("0x" + QString::number(nvme_bar_info.cmbloc, 16));
            regs_tblItem[11][1].setText("0x" + QString::number(nvme_bar_info.cmbsz, 16));


            buflog.buf = (char*)&nvme_bar_info;
            buflog.len = sizeof(nvme_bar_info);
            DumpBuffer(buflog.buf, buflog.len, false);


            ::close(pci_fd);
        }
    }
}

int QNVMe::GetNsid(int ctrl, int index)
{
    int fd;
    int nsid = 0;
    char buf[64] = {0};
    char path[512];

    sprintf(path, "/sys/class/nvme/nvme%d/nvme%dn%d/nsid", ctrl, ctrl, index);

    fd = open(path, O_RDONLY);

    if (fd != -1)
    {
        read(fd, &buf[0], 1);
        nsid = atoi(buf);
        ::close(fd);
    }

    return nsid;
}

void QNVMe::InitAdminOpTbl(void)
{
    admin_op_tbl[0]  = nvme_admin_delete_sq;
    admin_op_tbl[1]  = nvme_admin_create_sq;
    admin_op_tbl[2]  = nvme_admin_get_log_page;
    admin_op_tbl[3]  = nvme_admin_delete_cq;
    admin_op_tbl[4]  = nvme_admin_create_cq;
    admin_op_tbl[5]  = nvme_admin_identify;
    admin_op_tbl[6]  = nvme_admin_abort_cmd;
    admin_op_tbl[7]  = nvme_admin_set_features;
    admin_op_tbl[8]  = nvme_admin_get_features;
    admin_op_tbl[9]  = nvme_admin_async_event;
    admin_op_tbl[10] = nvme_admin_activate_fw;
    admin_op_tbl[11] = nvme_admin_download_fw;
    admin_op_tbl[12] = nvme_admin_format_nvm;
    admin_op_tbl[13] = nvme_admin_security_send;
    admin_op_tbl[14] = nvme_admin_security_recv;
    admin_op_tbl[15] = nvme_admin_vendor_specific;
    admin_op_tbl[16] = nvme_admin_vsc_nondata;
    admin_op_tbl[17] = nvme_admin_vsc_read;
    admin_op_tbl[18] = nvme_admin_vsc_write;
}

void QNVMe::InitNVMeOpTbl(void)
{
    nvme_op_tbl[0]   = nvme_cmd_flush;
    nvme_op_tbl[1]   = nvme_cmd_write;
    nvme_op_tbl[2]   = nvme_cmd_read;
    nvme_op_tbl[3]   = nvme_cmd_write_uncor;
    nvme_op_tbl[4]   = nvme_cmd_compare;
    nvme_op_tbl[5]   = nvme_cmd_write_zeroes;
    nvme_op_tbl[6]   = nvme_cmd_dsm;
    nvme_op_tbl[7]   = nvme_cmd_resv_register;
    nvme_op_tbl[8]   = nvme_cmd_resv_report;
    nvme_op_tbl[9]   = nvme_cmd_resv_acquire;
    nvme_op_tbl[10]  = nvme_cmd_resv_release;
    nvme_op_tbl[11]  = 0x0;
}

void QNVMe::InitTableWidgets(void)
{
    int i, j;

    ui->tbw_idctrl->setColumnCount(2);
    ui->tbw_idctrl->setRowCount(NUM_OF_ID_CTRL_ROW);
    ui->tbw_idctrl->setColumnWidth(0, 408);
    ui->tbw_idctrl->setColumnWidth(1, ui->tbw_idctrl->width() - 445);

    for (i = 0; i < NUM_OF_ID_CTRL_ROW; i++)
    {
       ui->tbw_idctrl->setRowHeight(i, 20);

       for (j = 0; j < 2; j++)
       {
           ui->tbw_idctrl->setItem(i, j, &id_tblItem[i][j]);
       }
    }

    id_tblItem[0][0].setText("PCI Vendor ID (VID)");
    id_tblItem[1][0].setText("PCI Subsystem Vendor ID (SSVID)");
    id_tblItem[2][0].setText("Serial Number (SN)");
    id_tblItem[3][0].setText("Model Number (MN)");
    id_tblItem[4][0].setText("Firmware Revision (FR)");
    id_tblItem[5][0].setText("Recommended Arbitration Burst (RAB)");
    id_tblItem[6][0].setText("IEEE OUI Identifier (IEEE)");
    id_tblItem[7][0].setText("CMIC");
    id_tblItem[8][0].setText("Maximum Data Transfer Size (MDTS)");
    id_tblItem[9][0].setText("Controller ID (CNTLID)");
    id_tblItem[10][0].setText("Version (VER)");
    id_tblItem[11][0].setText("RTD3 Resume Latency (RTD3R)");
    id_tblItem[12][0].setText("RTD3 Entry Latency (RTD3E)");
    id_tblItem[13][0].setText("Optional Asynchronous Events Supported (OAES)");
    id_tblItem[14][0].setText("Optional Admin Command Support (OACS)");
    id_tblItem[15][0].setText("Abort Command Limit (ACL)");
    id_tblItem[16][0].setText("Asynchronous Event Request Limit (AERL)");
    id_tblItem[17][0].setText("Firmware Updates (FRMW)");
    id_tblItem[18][0].setText("Log Page Attributes (LPA)");
    id_tblItem[19][0].setText("Error Log Page Entries (ELPE)");
    id_tblItem[20][0].setText("Number of Power States Support (NPSS)");
    id_tblItem[21][0].setText("Admin Vendor Specific Command Configuration (AVSCC)");
    id_tblItem[22][0].setText("Autonomous Power State Transition Attributes (APSTA)");
    id_tblItem[23][0].setText("Warning Composite Temperature Threshold (WCTEMP)");
    id_tblItem[24][0].setText("Critical Composite Temperature Threshold (CCTEMP)");
    id_tblItem[25][0].setText("Maximum Time for Firmware Activation (MTFA)");
    id_tblItem[26][0].setText("Host Memory Buffer Preferred Size (HMPRE)");
    id_tblItem[27][0].setText("Host Memory Buffer Minimum Size (HMMIN)");
    id_tblItem[28][0].setText("Total NVM Capacity (TNVMCAP)");
    id_tblItem[29][0].setText("Unallocated NVM Capacity (UNVMCAP)");
    id_tblItem[30][0].setText("Replay Protected Memory Block Support (RPMBS)");
    id_tblItem[31][0].setText("Submission Queue Entry Size (SQES)");
    id_tblItem[32][0].setText("Completion Queue Entry Size (CQES)");
    id_tblItem[33][0].setText("Number of Namespaces (NN)");
    id_tblItem[34][0].setText("Optional NVM Command Support (ONCS)");
    id_tblItem[35][0].setText("Fused Operation Support (FUSES)");
    id_tblItem[36][0].setText("Format NVM Attributes (FNA)");
    id_tblItem[37][0].setText("Volatile Write Cache (VWC)");
    id_tblItem[38][0].setText("Atomic Write Unit Normal (AWUN)");
    id_tblItem[39][0].setText("Atomic Write Unit Power Fail (AWUPF)");
    id_tblItem[40][0].setText("Atomic Compare & Write Unit (ACWU)");
    id_tblItem[41][0].setText("SGL Support (SGLS)");

    ui->tbw_idns->setColumnCount(2);
    ui->tbw_idns->setRowCount(NUM_OF_ID_NS_ROW);
    ui->tbw_idns->setColumnWidth(0, 408);
    ui->tbw_idns->setColumnWidth(1, ui->tbw_idns->width() - 445);

    for (i = 0; i < NUM_OF_ID_NS_ROW; i++)
    {
       ui->tbw_idns->setRowHeight(i, 20);

       for (j = 0; j < 2; j++)
       {
           ui->tbw_idns->setItem(i, j, &id_nsItem[i][j]);
       }
    }

    id_nsItem[0][0].setText("Namespace Size (NSZE)");
    id_nsItem[1][0].setText("Namespace Capacity (NCAP)");
    id_nsItem[2][0].setText("Namespace Utilization (NUSE)");
    id_nsItem[3][0].setText("Namespace Features (NSFEAT)");
    id_nsItem[4][0].setText("Number of LBA Formats (NLBAF)");
    id_nsItem[5][0].setText("Formatted LBA Size (FLBAS)");
    id_nsItem[6][0].setText("Metadata Capabilities (MC)");
    id_nsItem[7][0].setText("End-to-end Data Protection Capabilities (DPC)");
    id_nsItem[8][0].setText("End-to-end Data Protection Type Settings (DPS)");
    id_nsItem[9][0].setText("NMIC");
    id_nsItem[10][0].setText("Reservation Capabilities (RESCAP)");
    id_nsItem[11][0].setText("Format Progress Indicator (FPI)");
    id_nsItem[12][0].setText("Namespace Atomic Write Unit Normal (NAWUN)");
    id_nsItem[13][0].setText("Namespace Atomic Write Unit Power Fail (NAWUPF)");
    id_nsItem[14][0].setText("Namespace Atomic Compare & Write Unit (NACWU)");
    id_nsItem[15][0].setText("Namespace Atomic Boundary Size Normal (NABSN)");
    id_nsItem[16][0].setText("Namespace Atomic Boundary Offset (NABO)");
    id_nsItem[17][0].setText("Namespace Atomic Boundary Size Power Fail (NABSPF)");
    id_nsItem[18][0].setText("NVM Capacity (NVMCAP)");
    id_nsItem[19][0].setText("Namespace Globally Unique Identifier (NGUID)");
    id_nsItem[20][0].setText("IEEE Extended Unique Identifier (EUI64)");
    id_nsItem[21][0].setText("LBA Format 0 Support (LBAF0)");
    id_nsItem[22][0].setText("LBA Format 1 Support (LBAF1)");

    ui->tbw_smart->setColumnCount(2);
    ui->tbw_smart->setRowCount(NUM_OF_SMART_ROW);
    ui->tbw_smart->setColumnWidth(0, 308);
    ui->tbw_smart->setColumnWidth(1, ui->tbw_idctrl->width()- 345);

    for (i = 0; i < NUM_OF_SMART_ROW; i++)
    {
       ui->tbw_smart->setRowHeight(i, 20);

       for (j = 0; j < 2; j++)
       {
           ui->tbw_smart->setItem(i, j, &smart_tblItem[i][j]);
       }
    }

    smart_tblItem[0][0].setText("Critical warning");
    smart_tblItem[1][0].setText("Temperature");
    smart_tblItem[2][0].setText("Available spare");
    smart_tblItem[3][0].setText("Available spare Threshold");
    smart_tblItem[4][0].setText("Percentage used");
    smart_tblItem[5][0].setText("Data units read");
    smart_tblItem[6][0].setText("Data untis written");
    smart_tblItem[7][0].setText("Host read commands");
    smart_tblItem[8][0].setText("Host write commands");
    smart_tblItem[9][0].setText("Controller busy time");
    smart_tblItem[10][0].setText("Power cycles");
    smart_tblItem[11][0].setText("Power on hours");
    smart_tblItem[12][0].setText("Unsafe shutdowns");
    smart_tblItem[13][0].setText("Media Errors");
    smart_tblItem[14][0].setText("Num of error log entries");

    ui->tbw_error->setColumnCount(2);
    ui->tbw_error->setRowCount(NUM_OF_ERROR_ROW);
    ui->tbw_error->setColumnWidth(0, 300);
    ui->tbw_error->setColumnWidth(1, ui->tbw_error->width()- 330);

    for (i = 0; i < NUM_OF_ERROR_ROW; i++)
    {
       ui->tbw_error->setRowHeight(i, 20);

       for (j = 0; j < 2; j++)
       {
           ui->tbw_error->setItem(i, j, &error_tblItem[i][j]);
       }
    }

    error_tblItem[0][0].setText("Entry");
    error_tblItem[1][0].setText("Error Count");
    error_tblItem[2][0].setText("Submission Queue ID");
    error_tblItem[3][0].setText("Command ID");
    error_tblItem[4][0].setText("Status Field");
    error_tblItem[5][0].setText("Parameter Error Location");
    error_tblItem[6][0].setText("LBA");
    error_tblItem[7][0].setText("Namespace");
    error_tblItem[8][0].setText("Vendor Specific Information Available");
    error_tblItem[9][0].setText("Command Specific Information");

    ui->tbw_fw->setColumnCount(2);
    ui->tbw_fw->setRowCount(NUM_OF_FW_ROW);
    ui->tbw_fw->setColumnWidth(0, 300);
    ui->tbw_fw->setColumnWidth(1, ui->tbw_fw->width()- 330);

    for (i = 0; i < NUM_OF_FW_ROW; i++)
    {
       ui->tbw_fw->setRowHeight(i, 20);

       for (j = 0; j < 2; j++)
       {
           ui->tbw_fw->setItem(i, j, &fw_tblItem[i][j]);
       }
    }

    fw_tblItem[0][0].setText("Active Firmware Info (AFI)");
    fw_tblItem[1][0].setText("Firmware Revision for Slot 1 (FRS1)");
    fw_tblItem[2][0].setText("Firmware Revision for Slot 2 (FRS2)");
    fw_tblItem[3][0].setText("Firmware Revision for Slot 3 (FRS3)");
    fw_tblItem[4][0].setText("Firmware Revision for Slot 4 (FRS4)");
    fw_tblItem[5][0].setText("Firmware Revision for Slot 5 (FRS5)");
    fw_tblItem[6][0].setText("Firmware Revision for Slot 6 (FRS6)");
    fw_tblItem[7][0].setText("Firmware Revision for Slot 7 (FRS7)");

    ui->tbw_regs->setColumnCount(2);
    ui->tbw_regs->setRowCount(NUM_OF_REGS_ROW);
    ui->tbw_regs->setColumnWidth(0, 100);
    ui->tbw_regs->setColumnWidth(1, ui->tbw_regs->width()- 130);

    for (i = 0; i < NUM_OF_REGS_ROW; i++)
    {
       ui->tbw_regs->setRowHeight(i, 20);

       for (j = 0; j < 2; j++)
       {
           ui->tbw_regs->setItem(i, j, &regs_tblItem[i][j]);
       }
    }

    regs_tblItem[0][0].setText("cap");
    regs_tblItem[1][0].setText("version");
    regs_tblItem[2][0].setText("intms");
    regs_tblItem[3][0].setText("intmc");
    regs_tblItem[4][0].setText("cc");
    regs_tblItem[5][0].setText("csts");
    regs_tblItem[6][0].setText("nssr");
    regs_tblItem[7][0].setText("aqa");
    regs_tblItem[8][0].setText("asq");
    regs_tblItem[9][0].setText("acq");
    regs_tblItem[10][0].setText("cmbloc");
    regs_tblItem[11][0].setText("cmbsz");
}

int QNVMe::nvme_ioctl(int cmdcode, struct nvme_passthru_cmd *cmd, char* buf, bool output)
{
    int ret;

    ret = ioctl(fd, cmdcode, cmd);

    ui->le_ioctrl_status->setText(GetStatusString(ret));
    ui->te_output->clear();

    if ((ret == 0) && output)
    {
        if (cmdcode == NVME_IOCTL_SUBMIT_IO)
        {
            struct nvme_user_io* io;

            io = (struct nvme_user_io*)cmd;
            buflog.len = (io->nblocks + 1) * 512;
        }
        else
        {
            buflog.len = cmd->data_len;

            #if 0
            if ((cmd->data_len == 0))
            {
                memcpy((void*)buf, (void*)&cmd->result, 4);
                buflog.len = 4;

                printf("result[%08X]\n", cmd->result);
            }
            #endif

            if (cmd->opcode == nvme_admin_set_features || cmd->opcode == nvme_admin_get_features)
            {
                DumpBuffer((char*)&cmd->result ,4, false);
            }
        }

        buflog.buf = buf;

        DumpBuffer(buflog.buf, buflog.len, true);
    }

    if (ret)
    {
        ui->le_ioctrl_status->setStyleSheet("QLineEdit{background: rgb(255, 69, 0);}");
    }
    else
    {
        ui->le_ioctrl_status->setStyleSheet("QLineEdit{background: green;}");
    }

    return ret;
}

void QNVMe::DeviceDiscovery(void)
{
    char filepath[80];
    int cntId;
    int idx;

    dev_info.curr = 0;
    dev_info.cnt  = 0;

    for (idx = 0; idx < ui->cb_device->count(); idx++)
    {
        ui->cb_device->removeItem(idx);
    }

    cntId = 0;
    while (dev_info.cnt < MAX_DEV_NUM)
    {
        sprintf(filepath, "/dev/nvme%d", cntId);

        fd = open(filepath, O_RDONLY);

        if (fd < 0)
        {
            break;
        }
        else
        {
            Identify(0, CNS_IDFY_CONTROLLER);

            if (id_ctrl.ver >= NVME_VS(1, 1))
            {
                Identify(0, CNS_IDFY_ACTIVE_NAMESPACE_ID);

                for (idx = 0; idx < 1024; idx++)
                {
                    if (namesapce_list[idx])
                    {
                        sprintf(filepath, "/dev/nvme%dn%d", cntId, namesapce_list[idx]);
                        dev_info.dev_list[dev_info.cnt].devid = cntId;
                        dev_info.dev_list[dev_info.cnt].nsid  = namesapce_list[idx];
                        dev_info.cnt++;

                        ui->cb_device->addItem(filepath);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                int nsid;

                for (idx = 1; idx <= (int)id_ctrl.nn; idx++)
                {
                    nsid = GetNsid(cntId, idx);
                    if (nsid)
                    {
                        sprintf(filepath, "/dev/nvme%dn%d", cntId, idx);
                        dev_info.dev_list[dev_info.cnt].devid = cntId;
                        dev_info.dev_list[dev_info.cnt].nsid  = nsid;
                        dev_info.cnt++;
                    }

                    ui->cb_device->addItem(filepath);
                }
            }

            ::close(fd);
        }

        cntId++;
    }

    if (dev_info.cnt)
    {
        dev_info.curr = 0;
        memset(filepath, 0x00, 80);
        sprintf(filepath, "/dev/nvme%dn%d", dev_info.dev_list[0].devid , dev_info.dev_list[0].nsid);

        fd = open(filepath, O_RDONLY);

        Identify(0, CNS_IDFY_CONTROLLER);
        Identify(1, CNS_IDFY_NAMESPACE);

        ShowDiskInfo();

        ui->statusBar->showMessage("Device Found:"+QString::fromLocal8Bit(filepath, strlen(filepath)));
    }
    else
    {
        ui->statusBar->showMessage("No Device Found!");
    }
}

void QNVMe::ShowDiskInfo(void)
{
    int lbaf;

    lbaf = id_ns.flbas & 0xF;

    if (((id_ns.ncap << id_ns.lbaf[lbaf].ds) >> 30) == 0)
    {
        ui->le_devInfo->setText(QString::fromLocal8Bit(id_ctrl.mn, 40) + "| " + QString::number((id_ns.ncap << id_ns.lbaf[lbaf].ds) >> 20) + " MB");
    }
    else
    {
        ui->le_devInfo->setText(QString::fromLocal8Bit(id_ctrl.mn, 40) + "| " + QString::number((id_ns.ncap << id_ns.lbaf[lbaf].ds) >> 30) + " GB");
    }
}

int QNVMe::Identify(int nsid, int cns)
{
    int ret;
    char str[80];
    struct nvme_passthru_cmd cmd;

    // NVMe Identify commannn
    memset(&cmd, 0, sizeof(cmd));
    cmd.opcode   = nvme_admin_identify;
    cmd.nsid     = nsid;
    cmd.data_len = 4096;
    cmd.cdw10    = cns;

    switch (cns)
    {
        case CNS_IDFY_NAMESPACE:
        {
            memset(&id_ns, 0, sizeof(id_ns));
            cmd.addr = (unsigned long)&id_ns;
            ret = nvme_ioctl(NVME_IOCTL_ADMIN_CMD, &cmd, (char*)&id_ns, true);

            if (ret == 0)
            {
                id_nsItem[0][1].setText("0x"+QString::number(id_ns.nsze, 16));
                id_nsItem[1][1].setText("0x"+QString::number(id_ns.ncap, 16));
                id_nsItem[2][1].setText("0x"+QString::number(id_ns.nuse, 16));
                id_nsItem[3][1].setText("0x"+QString::number(id_ns.nsfeat, 16));
                id_nsItem[4][1].setText(QString::number(id_ns.nlbaf));
                id_nsItem[5][1].setText("0x"+QString::number(id_ns.flbas, 16));
                id_nsItem[6][1].setText("0x"+QString::number(id_ns.mc, 16));
                id_nsItem[7][1].setText("0x"+QString::number(id_ns.dpc, 16));
                id_nsItem[8][1].setText("0x"+QString::number(id_ns.dps, 16));
                id_nsItem[9][1].setText("0x"+QString::number(id_ns.nmic, 16));
                id_nsItem[10][1].setText("0x"+QString::number(id_ns.rescap, 16));
                id_nsItem[11][1].setText("0x"+QString::number(id_ns.fpi, 16));
                id_nsItem[12][1].setText(QString::number(id_ns.nawun));
                id_nsItem[13][1].setText(QString::number(id_ns.nawupf));
                id_nsItem[14][1].setText(QString::number(id_ns.nacwu));
                id_nsItem[15][1].setText(QString::number(id_ns.nabsn));
                id_nsItem[16][1].setText(QString::number(id_ns.nabo));
                id_nsItem[17][1].setText(QString::number(id_ns.nabspf));
                id_nsItem[18][1].setText(QString::number(IntToDouble(id_ns.nvmcap, 16)));

                IntToByte(id_ns.nguid, str, 16);
                id_nsItem[19][1].setText(QString::fromLocal8Bit(str, strlen(str)));

                IntToByte(id_ns.eui64, str, 8);
                id_nsItem[20][1].setText(QString::fromLocal8Bit(str, strlen(str)));

                id_nsItem[21][1].setText("ms:"+QString::number(id_ns.lbaf[0].ms)+" ds:"+QString::number(id_ns.lbaf[0].ds)+" rp:"+QString::number(id_ns.lbaf[0].rp));
                id_nsItem[22][1].setText("ms:"+QString::number(id_ns.lbaf[1].ms)+" ds:"+QString::number(id_ns.lbaf[1].ds)+" rp:"+QString::number(id_ns.lbaf[1].rp));
            }

            break;
        }
        case CNS_IDFY_CONTROLLER:
        {
            memset(&id_ctrl, 0, sizeof(id_ctrl));
            cmd.addr = (unsigned long)&id_ctrl;
            ret = nvme_ioctl(NVME_IOCTL_ADMIN_CMD, &cmd, (char*)&id_ctrl, true);

            if (ret == 0)
            {
                id_tblItem[0][1].setText("0x"+QString::number(id_ctrl.vid, 16));
                id_tblItem[1][1].setText("0x"+QString::number(id_ctrl.ssvid, 16));
                id_tblItem[2][1].setText(QString::fromLocal8Bit(id_ctrl.sn, 20));
                id_tblItem[3][1].setText(QString::fromLocal8Bit(id_ctrl.mn, 40));
                id_tblItem[4][1].setText(QString::fromLocal8Bit(id_ctrl.fr, 8));
                id_tblItem[5][1].setText(QString::number(id_ctrl.rab));
                id_tblItem[6][1].setText("0x"+QString::number(id_ctrl.ieee[0], 16) + QString::number(id_ctrl.ieee[1], 16) + QString::number(id_ctrl.ieee[2], 16));
                id_tblItem[7][1].setText(QString::number(id_ctrl.cmic));
                id_tblItem[8][1].setText(QString::number(id_ctrl.mdts));
                id_tblItem[9][1].setText("0x" + QString::number(id_ctrl.cntlid, 16));
                id_tblItem[10][1].setText("0x"+QString::number(id_ctrl.ver, 16));
                id_tblItem[11][1].setText(QString::number(id_ctrl.rtd3r));
                id_tblItem[12][1].setText(QString::number(id_ctrl.rtd3e));
                id_tblItem[13][1].setText("0x" + QString::number(id_ctrl.oaes, 16));
                id_tblItem[14][1].setText("0x" + QString::number(id_ctrl.oacs, 16));
                id_tblItem[15][1].setText(QString::number(id_ctrl.acl));
                id_tblItem[16][1].setText(QString::number(id_ctrl.aerl));
                id_tblItem[17][1].setText("0x" + QString::number(id_ctrl.frmw));
                id_tblItem[18][1].setText("0x" + QString::number(id_ctrl.lpa));
                id_tblItem[19][1].setText(QString::number(id_ctrl.elpe));
                id_tblItem[20][1].setText(QString::number(id_ctrl.npss));
                id_tblItem[21][1].setText("0x" + QString::number(id_ctrl.avscc));
                id_tblItem[22][1].setText("0x" + QString::number(id_ctrl.apsta));
                id_tblItem[23][1].setText(QString::number(id_ctrl.wctemp));
                id_tblItem[24][1].setText(QString::number(id_ctrl.cctemp));
                id_tblItem[25][1].setText(QString::number(id_ctrl.mtfa));
                id_tblItem[26][1].setText(QString::number(id_ctrl.hmpre));
                id_tblItem[27][1].setText(QString::number(id_ctrl.hmmin));
                id_tblItem[28][1].setText(QString::number(IntToDouble(id_ctrl.tnvmcap, 16)));
                id_tblItem[29][1].setText(QString::number(IntToDouble(id_ctrl.unvmcap, 16)));
                id_tblItem[30][1].setText(QString::number(id_ctrl.rpmbs));
                id_tblItem[31][1].setText("0x" + QString::number(id_ctrl.sqes, 16));
                id_tblItem[32][1].setText("0x" + QString::number(id_ctrl.cqes, 16));
                id_tblItem[33][1].setText(QString::number(id_ctrl.nn));
                id_tblItem[34][1].setText("0x" + QString::number(id_ctrl.oncs));
                id_tblItem[35][1].setText("0x" + QString::number(id_ctrl.fuses));
                id_tblItem[36][1].setText("0x" + QString::number(id_ctrl.fna));
                id_tblItem[37][1].setText("0x" + QString::number(id_ctrl.vwc));
                id_tblItem[38][1].setText(QString::number(id_ctrl.awun));
                id_tblItem[39][1].setText(QString::number(id_ctrl.awupf));
                id_tblItem[40][1].setText(QString::number(id_ctrl.acwu));
                id_tblItem[41][1].setText("0x" + QString::number(id_ctrl.sgls));
            }
            break;
        }
        case CNS_IDFY_ACTIVE_NAMESPACE_ID:
        {
            memset(namesapce_list, 0, sizeof(namesapce_list));
            cmd.addr = (unsigned long)namesapce_list;
            ret = nvme_ioctl(NVME_IOCTL_ADMIN_CMD, &cmd, (char*)namesapce_list, true);

            break;
        }
        case CNS_IDFY_CONTAIN_NAMESPACE_ID:
        {
            break;
        }
        case CNS_IDFY_CONTAIN_NAMESPACE:
        {
            break;
        }
        case CNS_IDFY_CONTROLLER_LIST_ATTACH:
        {
            break;
        }
        case CNS_IDFY_CONTROLLER_LIST:
        {
            break;
        }
        default:
            break;
    }

    return 0;
}

void QNVMe::IntToByte(__u8 *list, char* buf, int len)
{
    int i;
    memset(buf, 0, sizeof(len));
    for (i = 0; i < len; i++)
    {
        sprintf(buf, "%s%02X", buf, list[i]);
    }
}

u_int64_t QNVMe::ByteToInteger(const char* buf, int base)
{
    char c;
    int len, i;
    u_int64_t num = 0, mul = 1;

    len = strlen(buf);

    for (i = 0; i < len; i++)
    {
        c = buf[len - 1 - i];
        if (c >= '0' && c <= '9')
        {
            num += ((buf[len - 1 - i] - '0') * mul);
        }
        else if (c >= 'a' && c <= 'f')
        {
            num += ((buf[len - 1 - i] - 'a' + 10) * mul);
        }
        else if (c >= 'A' && c <= 'F')
        {
            num += ((buf[len - 1 - i] - 'A' + 10) * mul);
        }

        mul *= base;
    }

    return num;
}

double QNVMe::IntToDouble(__u8* list, int len)
{
    int i;
    double sum = 0;

    for (i = 0; i < len; i++)
    {
        sum *= 256;
        sum += list[len - 1 - i];
    }

    return sum;
}

char* QNVMe::FWLogToString(__u64 fw)
{
    static char ret[9];
    char *c = (char *)&fw;
    int i;

    for (i = 0; i < 8; i++)
        ret[i] = c[i] >= '!' && c[i] <= '~' ? c[i] : ' ';
    ret[i] = '\0';
    return ret;
}

void QNVMe::FWLog(int nsid)
{
    int i;
    int ret;
    struct nvme_passthru_cmd cmd;

    // NVMe Identify commannn
    memset(&cmd, 0, sizeof(cmd));
    cmd.opcode = nvme_admin_get_log_page;
    cmd.nsid = nsid;
    cmd.addr = (unsigned long)&fw_log;
    cmd.data_len = sizeof(fw_log);
    cmd.cdw10 = 0x3 | (((sizeof(fw_log) / 4) - 1) << 16);

    ret = nvme_ioctl(NVME_IOCTL_ADMIN_CMD, &cmd, (char*)&fw_log, true);

    if (ret == 0)
    {
        fw_tblItem[0][1].setText("0x" + QString::number(fw_log.afi, 16));

        for (i = 1; i <= 7; i++)
        {
            fw_tblItem[i][1].setText(FWLogToString(fw_log.frs[i-1]));
        }
    }
}

void QNVMe::SMARTLog(int nsid)
{
    int ret;
    struct nvme_passthru_cmd cmd;

    // NVMe Identify commannn
    memset(&cmd, 0, sizeof(cmd));
    cmd.opcode = nvme_admin_get_log_page;
    cmd.nsid = nsid;
    cmd.addr = (unsigned long)&smart_log;
    cmd.data_len = sizeof(smart_log);
    cmd.cdw10 = 0x2 | (((sizeof(smart_log) / 4) - 1) << 16);

    ret = nvme_ioctl(NVME_IOCTL_ADMIN_CMD, &cmd, (char*)&smart_log, true);

    if (ret == 0)
    {
        smart_tblItem[0][1].setText(QString::number(smart_log.critical_warning));
        smart_tblItem[1][1].setText(QString::number(((smart_log.temperature[1] << 8) | (smart_log.temperature[0])) - 273) + " C");
        smart_tblItem[2][1].setText(QString::number(smart_log.avail_spare) + " \%");
        smart_tblItem[3][1].setText(QString::number(smart_log.spare_thresh) + " \%");
        smart_tblItem[4][1].setText(QString::number(smart_log.percent_used) + " \%");
        smart_tblItem[5][1].setText(QString::number(IntToDouble(smart_log.data_units_read, 16)));
        smart_tblItem[6][1].setText(QString::number(IntToDouble(smart_log.data_units_written, 16)));
        smart_tblItem[7][1].setText(QString::number(IntToDouble(smart_log.host_reads, 16)));
        smart_tblItem[8][1].setText(QString::number(IntToDouble(smart_log.host_writes, 16)));
        smart_tblItem[9][1].setText(QString::number(IntToDouble(smart_log.ctrl_busy_time, 16)));
        smart_tblItem[10][1].setText(QString::number(IntToDouble(smart_log.power_cycles, 16)));
        smart_tblItem[11][1].setText(QString::number(IntToDouble(smart_log.power_on_hours, 16)));
        smart_tblItem[12][1].setText(QString::number(IntToDouble(smart_log.unsafe_shutdowns, 16)));
        smart_tblItem[13][1].setText(QString::number(IntToDouble(smart_log.media_errors, 16)));
        smart_tblItem[14][1].setText(QString::number(IntToDouble(smart_log.num_err_log_entries, 16)));
    }
}

void QNVMe::ErrorLog(int nsid, int index)
{
    int ret;
    struct nvme_passthru_cmd cmd;

    // NVMe Identify commannn
    memset(&cmd, 0, sizeof(cmd));
    cmd.opcode = nvme_admin_get_log_page;
    cmd.nsid = nsid;
    cmd.addr = (unsigned long)&error_log;
    cmd.data_len = sizeof(error_log);
    cmd.cdw10 = 0x1 | (((sizeof(struct nvme_error_log_page) * (id_ctrl.elpe + 1) / 4) - 1) << 16);

    ret = nvme_ioctl(NVME_IOCTL_ADMIN_CMD, &cmd, (char*)&error_log, true);

    if (ret == 0)
    {
        error_tblItem[0][1].setText(QString::number(index));
        error_tblItem[1][1].setText(QString::number(error_log[index].error_count));
        error_tblItem[2][1].setText("0x" + QString::number(error_log[index].sqid, 16));
        error_tblItem[3][1].setText("0x" + QString::number(error_log[index].cmdid, 16));
        error_tblItem[4][1].setText(QString::number(error_log[index].status_field));
        error_tblItem[5][1].setText("0x" + QString::number(error_log[index].parm_error_location, 16));
        error_tblItem[6][1].setText(QString::number(error_log[index].lba));
        error_tblItem[7][1].setText(QString::number(error_log[index].nsid));
        error_tblItem[8][1].setText(QString::number(error_log[index].vs));
        error_tblItem[9][1].setText("0x" + QString::number(IntToDouble(error_log[index].csi, 16)));
    }
}

const char* QNVMe::GetStatusString(unsigned int status)
{
    switch (status & 0x3ff) {
        case NVME_SC_SUCCESS:           return "SUCCESS";
        case NVME_SC_INVALID_OPCODE:    return "INVALID_OPCODE";
        case NVME_SC_INVALID_FIELD:     return "INVALID_FIELD";
        case NVME_SC_CMDID_CONFLICT:    return "CMDID_CONFLICT";
        case NVME_SC_DATA_XFER_ERROR:	return "DATA_XFER_ERROR";
        case NVME_SC_POWER_LOSS:        return "POWER_LOSS";
        case NVME_SC_INTERNAL:          return "INTERNAL";
        case NVME_SC_ABORT_REQ:         return "ABORT_REQ";
        case NVME_SC_ABORT_QUEUE:       return "ABORT_QUEUE";
        case NVME_SC_FUSED_FAIL:        return "FUSED_FAIL";
        case NVME_SC_FUSED_MISSING:     return "FUSED_MISSING";
        case NVME_SC_INVALID_NS:        return "INVALID_NS";
        case NVME_SC_CMD_SEQ_ERROR:     return "CMD_SEQ_ERROR";
        case NVME_SC_LBA_RANGE:         return "LBA_RANGE";
        case NVME_SC_CAP_EXCEEDED:      return "CAP_EXCEEDED";
        case NVME_SC_NS_NOT_READY:      return "NS_NOT_READY";
        case NVME_SC_CQ_INVALID:        return "CQ_INVALID";
        case NVME_SC_QID_INVALID:       return "QID_INVALID";
        case NVME_SC_QUEUE_SIZE:        return "QUEUE_SIZE";
        case NVME_SC_ABORT_LIMIT:       return "ABORT_LIMIT";
        case NVME_SC_ABORT_MISSING:     return "ABORT_MISSING";
        case NVME_SC_ASYNC_LIMIT:       return "ASYNC_LIMIT";
        case NVME_SC_FIRMWARE_SLOT:     return "FIRMWARE_SLOT";
        case NVME_SC_FIRMWARE_IMAGE:	return "FIRMWARE_IMAGE";
        case NVME_SC_INVALID_VECTOR:	return "INVALID_VECTOR";
        case NVME_SC_INVALID_LOG_PAGE:	return "INVALID_LOG_PAGE";
        case NVME_SC_INVALID_FORMAT:	return "INVALID_FORMAT";
        case NVME_SC_BAD_ATTRIBUTES:	return "BAD_ATTRIBUTES";
        case NVME_SC_WRITE_FAULT:       return "WRITE_FAULT";
        case NVME_SC_READ_ERROR:        return "READ_ERROR";
        case NVME_SC_GUARD_CHECK:       return "GUARD_CHECK";
        case NVME_SC_APPTAG_CHECK:      return "APPTAG_CHECK";
        case NVME_SC_REFTAG_CHECK:      return "REFTAG_CHECK";
        case NVME_SC_COMPARE_FAILED:	return "COMPARE_FAILED";
        case NVME_SC_ACCESS_DENIED:     return "ACCESS_DENIED";
        default:                        return "Unknown";
    }
}

/*
 * Event Handlers
 *
 */

void QNVMe::onRefreshClicked(void)
{
    if (Identify(0, CNS_IDFY_CONTROLLER) != 0)
    {
        emit ui->pb_umount->click();
        emit ui->pb_mount->click();
    }
}

void QNVMe::onMountClicked(void)
{
    externalProcess.start("dmesg -wH");
    system("sudo insmod /lib/modules/`uname -r`/kernel/drivers/nvme/host/nvme.ko");
    sleep(1);
    DeviceDiscovery();
}

void QNVMe::onUnmountClicked(void)
{
    int i;

    externalProcess.close();
    ::close(fd);
    system("sudo rmmod nvme.ko");
    ui->le_devInfo->setText("");
    ui->statusBar->showMessage("Device Unmount!");

    for (i = 0; i < ui->cb_device->count(); i++)
    {
        ui->cb_device->removeItem(i);
    }
}

void QNVMe::onAdminIndexChanged(int index)
{
    char str[40];

    sprintf(str, "0x%02X", admin_op_tbl[index] & 0xFF);
    ui->le_admin_op->setText(QString::fromLatin1(str));
    SetupAdminCmdWidgets(admin_op_tbl[index] & 0xFF);
}

void QNVMe::onNVMeIndexChanged(int index)
{
    char str[40];

    sprintf(str, "0x%02X", nvme_op_tbl[index] & 0xFF);
    ui->le_nvme_op->setText(QString::fromLatin1(str));
    SetupNVMeCmdWidgets(nvme_op_tbl[index] & 0xFF);
}

void QNVMe::onDeviceIndexChanged(int index)
{
    dev_info.curr = index;
    Identify(dev_info.dev_list[dev_info.curr].nsid, CNS_IDFY_NAMESPACE);

    ShowDiskInfo();
}

void QNVMe::onAdminProceedClicked(void)
{
    struct nvme_passthru_cmd cmd;

    memset(&cmd, 0x00, sizeof(cmd));

    cmd.opcode = admin_op_tbl[ui->cb_admin->currentIndex()];        // cdw0
    cmd.nsid   = ui->le_admin_nsid->text().toInt();                 // cdw1
    cmd.addr   = (unsigned long)tmpBuf;                             // cdw6,7

    cmd.metadata = ui->le_admin_metadata->text().toInt();
    cmd.metadata_len = ui->le_admin_metadatalen->text().toInt();
    cmd.cdw2   = ui->le_admin_cdw2->text().toInt();
    cmd.cdw3   = ui->le_admin_cdw3->text().toInt();
    cmd.cdw10  = ui->le_admin_cdw10->text().toInt();
    cmd.cdw11  = ui->le_admin_cdw11->text().toInt();
    cmd.cdw12  = ui->le_admin_cdw12->text().toInt();
    cmd.cdw13  = ui->le_admin_cdw13->text().toInt();
    cmd.cdw14  = ui->le_admin_cdw14->text().toInt();
    cmd.cdw15  = ui->le_admin_cdw15->text().toInt();

    AutoConfigAdminCmd(&cmd);

    ui->le_admin_datalen->setText(QString::number(cmd.data_len));
    ui->le_admin_addr->setText(QString::number(cmd.addr, 16));

    nvme_ioctl(NVME_IOCTL_ADMIN_CMD, &cmd, tmpBuf, true);
}

void QNVMe::DumpBuffer(char* buf, int len, int append)
{
    int i;
    char str[128];

    if (ui->rb_big->isChecked())
    {
        int j;

        memset(tmpRvsBuf, 0x00, len);
        for (i = 0; i < len / 4; i++)
        {
            for (j = 0; j < 4; j++)
            {
                tmpRvsBuf[4*i+j] = buf[4*i+3-j];
            }
        }

        buf = tmpRvsBuf;
    }

    if (!append)
    {
        ui->te_output->clear();
    }

    if (ui->rb_byte->isChecked())
    {
        int j;

        ui->te_output->append("           0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F");
        ui->te_output->append("----------------------------------------------------------");

        for (i = 0; i < len; i++)
        {

            if ((i & 0xf) == 0)
            {
                sprintf(str, "%08X:", i);
            }

            sprintf(str, "%s %02X", str, buf[i] & 0xFF);

            if (((i & 0xf) == 0xf) || (i == (len - 1)))
            {
                if (i != 0xf)
                {
                    for (j = 0; j < 15-i; j++)
                    {
                        sprintf(str, "%s   ", str);
                    }
                }

                sprintf(str, "%s | ", str);

                for (j = i - 15; j <= i; j++)
                {
                    if (buf[j] > 0x1F && buf[j] < 0x7F)
                    {
                        sprintf(str, "%s%c",str, buf[j] & 0xFF);
                    }
                    else
                    {
                        sprintf(str, "%s.", str);
                    }

                }

                ui->te_output->append(QString::fromLocal8Bit(str, strlen(str)));
            }
        }

        ui->te_output->append("----------------------------------------------------------");
    }
    else if(ui->rb_word->isChecked())
    {
        unsigned short* ptr;

        ptr = (unsigned short*)buf;
        ui->te_output->append("            0    1    2    3    4    5    6    7");
        ui->te_output->append("-------------------------------------------------");

        for (i = 0; i < len / 2; i++)
        {

            if ((i & 0x7) == 0)
            {
                sprintf(str, "%08X:", i * 2);
            }

            sprintf(str, "%s %04X",str, ptr[i] & 0xFFFF);

            if (((i & 0x7) == 0x7) || (i == (len / 2 - 1)))
            {
                strcat(str, "\0");
                ui->te_output->append(QString::fromLocal8Bit(str, strlen(str)));
            }
        }

        ui->te_output->append("-------------------------------------------------");
    }
    else
    {
        unsigned int* ptr;

        ptr = (unsigned int*)buf;
        ui->te_output->append("              0        1        2        3");
        ui->te_output->append("---------------------------------------------");

        for (i = 0; i < len / 4; i++)
        {

            if ((i & 0x3) == 0)
            {
                sprintf(str, "%08X:", i * 4);
            }

            sprintf(str, "%s %08X",str, ptr[i]);

            if (((i & 0x3) == 0x3) || (i == (len / 4 - 1)))
            {
                strcat(str, "\0");
                ui->te_output->append(QString::fromLocal8Bit(str, strlen(str)));
            }
        }

        ui->te_output->append("---------------------------------------------");
    }

    ui->te_output->moveCursor(QTextCursor::Start);
}

void QNVMe::SetupAdminCmdWidgets(int opcode)
{
    ui->pb_admin_proceed->setEnabled(true);
    ui->le_admin_nsid->setEnabled(true);
    ui->le_admin_cdw10->setEnabled(true);

    ui->le_admin_flags->setEnabled(false);
    ui->le_admin_rsvd->setEnabled(false);
    ui->le_admin_cdw2->setEnabled(false);
    ui->le_admin_cdw3->setEnabled(false);
    ui->le_admin_metadata->setEnabled(false);
    ui->le_admin_metadatalen->setEnabled(false);
    ui->le_admin_addr->setEnabled(false);
    ui->le_admin_datalen->setEnabled(false);
    ui->le_admin_cdw11->setEnabled(false);
    ui->le_admin_cdw12->setEnabled(false);
    ui->le_admin_cdw13->setEnabled(false);
    ui->le_admin_cdw14->setEnabled(false);
    ui->le_admin_cdw15->setEnabled(false);

    switch(opcode)
    {
        case nvme_admin_identify:
        case nvme_admin_get_log_page:
        case nvme_admin_format_nvm:
        case nvme_admin_security_send:
        case nvme_admin_get_features:
            break;
        case nvme_admin_set_features:
        case nvme_admin_security_recv:
        case nvme_admin_download_fw:
            ui->le_admin_cdw11->setEnabled(true);
            break;
        case nvme_admin_activate_fw:
            ui->le_admin_nsid->setEnabled(false);
            break;
        case nvme_admin_create_cq:
        case nvme_admin_delete_cq:
        case nvme_admin_create_sq:
        case nvme_admin_delete_sq:
        case nvme_admin_abort_cmd:
        case nvme_admin_async_event:
            ui->pb_admin_proceed->setEnabled(false);
            ui->le_admin_nsid->setEnabled(false);
            ui->le_admin_cdw10->setEnabled(false);
            break;
        case nvme_admin_vsc_nondata:
        case nvme_admin_vsc_read:
        case nvme_admin_vsc_write:
            ui->le_admin_cdw12->setEnabled(true);
            ui->le_admin_cdw13->setEnabled(true);
            ui->le_admin_cdw14->setEnabled(true);
            break;
        default:
            ui->le_admin_flags->setEnabled(true);
            ui->le_admin_rsvd->setEnabled(true);
            ui->le_admin_cdw2->setEnabled(true);
            ui->le_admin_cdw3->setEnabled(true);
            ui->le_admin_metadata->setEnabled(true);
            ui->le_admin_metadatalen->setEnabled(true);
            ui->le_admin_addr->setEnabled(true);
            ui->le_admin_datalen->setEnabled(true);
            ui->le_admin_cdw11->setEnabled(true);
            ui->le_admin_cdw12->setEnabled(true);
            ui->le_admin_cdw13->setEnabled(true);
            ui->le_admin_cdw14->setEnabled(true);
            ui->le_admin_cdw15->setEnabled(true);
            break;
    }
}

void QNVMe::SetupNVMeCmdWidgets(int opcode)
{
    ui->pb_nvme_proceed->setEnabled(true);
    ui->le_nvme_flags->setEnabled(true);

    ui->le_nvme_metadata->setEnabled(true);
    ui->le_nvme_slba->setEnabled(true);
    ui->le_nvme_length->setEnabled(true);
    ui->le_nvme_control->setEnabled(true);

    ui->le_nvme_dsmgmt->setEnabled(false);
    ui->le_nvme_reftag->setEnabled(false);
    ui->le_nvme_apptag->setEnabled(false);
    ui->le_nvme_appmask->setEnabled(false);
    ui->le_nvme_cmdid->setEnabled(false);
    ui->le_nvme_nsid->setEnabled(false);
    ui->le_nvme_prp1->setEnabled(false);
    ui->le_nvme_prp2->setEnabled(false);
    ui->le_nvme_rsvd2->setEnabled(false);

    switch(opcode)
    {
        case nvme_cmd_flush:
            ui->le_nvme_nsid->setEnabled(true);
            ui->le_nvme_flags->setEnabled(false);
            ui->le_nvme_metadata->setEnabled(false);
            ui->le_nvme_slba->setEnabled(false);
            ui->le_nvme_length->setEnabled(false);
            ui->le_nvme_control->setEnabled(false);
        case nvme_cmd_write:
        case nvme_cmd_read:
        case nvme_cmd_compare:
            break;
        case nvme_cmd_write_uncor:
        case nvme_cmd_dsm:
        case nvme_cmd_resv_acquire:
        case nvme_cmd_resv_register:
        case nvme_cmd_resv_release:
        case nvme_cmd_resv_report:
            ui->pb_nvme_proceed->setEnabled(false);
            break;
        default:
            ui->pb_nvme_proceed->setEnabled(false);
            break;
    }
}

void QNVMe::AutoConfigAdminCmd(struct nvme_passthru_cmd* cmd)
{
    switch(cmd->opcode)
    {
        case nvme_admin_get_features:
        case nvme_admin_set_features:
        {
            int fid;

            fid = cmd->cdw10 & 0xFF;

            if (fid == NVME_FEAT_LBA_RANGE)
            {
                cmd->data_len = 4096;
            }
            else
            {
                cmd->data_len = 0;
            }

            if ((fid == NVME_FEAT_AUTO_PST) && (cmd->opcode == nvme_admin_get_features))
            {
                cmd->data_len = 256;
            }
        }
            break;
        case nvme_admin_identify:
        case nvme_admin_get_log_page:
            cmd->data_len = 4096;
            break;
        case nvme_admin_vsc_nondata:
            break;
        case nvme_admin_vsc_read:
        case nvme_admin_vsc_write:
            cmd->data_len = 4096;
            break;
        default:
            cmd->data_len = ui->le_admin_datalen->text().toInt();
            break;
    }
}

void QNVMe::AutoConfigNVMeCmd(struct nvme_passthru_cmd* cmd)
{
    switch(cmd->opcode)
    {
        case nvme_cmd_flush:
        case nvme_cmd_write:
        case nvme_cmd_read:
        case nvme_cmd_compare:
        case nvme_cmd_write_uncor:
        case nvme_cmd_dsm:
        case nvme_cmd_resv_acquire:
        case nvme_cmd_resv_register:
        case nvme_cmd_resv_release:
        case nvme_cmd_resv_report:
            break;
        default:
            break;
    }
}

void QNVMe::onNVMeProceedClicked(void)
{
    struct nvme_passthru_cmd cmd;
    struct nvme_user_io io;

    switch (nvme_op_tbl[ui->cb_nvme->currentIndex()])
    {
        case nvme_cmd_read:
        case nvme_cmd_write:
        case nvme_cmd_compare:
            memset(&io, 0x00, sizeof(io));

            //memset(tmpBuf+512, 0xFF, 512);

            io.opcode  = nvme_op_tbl[ui->cb_nvme->currentIndex()];
            io.slba    = ui->le_nvme_slba->text().toInt();
            io.addr    = (unsigned long)tmpBuf;
            io.nblocks = ui->le_nvme_length->text().toInt();
            io.control = ui->le_nvme_control->text().toInt();
            io.dsmgmt  = ui->le_nvme_dsmgmt->text().toInt();
            io.reftag  = ui->le_nvme_reftag->text().toInt();
            io.apptag  = ui->le_nvme_apptag->text().toInt();
            io.appmask = ui->le_nvme_appmask->text().toInt();

            nvme_ioctl(NVME_IOCTL_SUBMIT_IO, (struct nvme_passthru_cmd*)&io, tmpBuf, true);
            break;
        case nvme_cmd_flush:
            memset(&cmd, 0x00, sizeof(cmd));
            cmd.opcode = nvme_op_tbl[ui->cb_nvme->currentIndex()];
            cmd.nsid   = ui->le_nvme_nsid->text().toInt();

            nvme_ioctl(NVME_IOCTL_IO_CMD, &cmd, tmpBuf, false);
            break;
        default:
            memset(&cmd, 0x00, sizeof(cmd));
            cmd.opcode = nvme_op_tbl[ui->cb_nvme->currentIndex()];

            //nvme_ioctl(NVME_IOCTL_IO_CMD, &cmd, tmpBuf, true);
            break;
    }
}

void QNVMe::onBrowseClicked(void)
{
    fw_bin_path = QFileDialog::getOpenFileName(this, tr("Open FW File"),  QDir::currentPath(), tr("FW (*.bin)"));
    ui->le_fw_path->setText(fw_bin_path);
}

void QNVMe::onBufFmtGroupClicked(void)
{
    if (buflog.buf)
    {
        DumpBuffer(buflog.buf, buflog.len, false);
    }
}

void QNVMe::onClearClicked(void)
{
    ui->te_output->clear();
}

void QNVMe::onSaveClicked(void)
{
    FILE* fp;
    char buf[256];

    QDateTime date(QDateTime::currentDateTime());

    output_path = QFileDialog::getSaveFileName(this, tr("Save Output File"),  QDir::currentPath()+"/"+date.toString("yyyyMMdd-hhmmss")+".bin", tr("File (*.bin)"));

    fp = fopen(output_path.toLatin1().data(), "wb+");

    if (fp != NULL)
    {
        fwrite(buflog.buf, buflog.len, sizeof(char), fp);

        fclose(fp);
    }

    if (ui->tab_main->currentIndex() < 5 || ui->tab_main->currentIndex() == MAIN_TAB_REGS)
    {
        sprintf(buf, "%s.csv", output_path.toLatin1().data());

        fp = fopen(output_path.replace(output_path.length()-4,4,".csv").toLatin1().data(), "w+");

        if (fp != NULL)
        {
            int i;

            switch(ui->tab_main->currentIndex())
            {
                case MAIN_TAB_ID_CTRL:
                    for (i = 0; i < NUM_OF_ID_CTRL_ROW; i++)
                    {
                        fprintf(fp, "%s,%s\n", id_tblItem[i][0].text().toLatin1().data(), id_tblItem[i][1].text().toLatin1().data());
                    }
                    break;
                case MAIN_TAB_ID_NS:
                    for (i = 0; i < NUM_OF_ID_NS_ROW; i++)
                    {
                        fprintf(fp, "%s,%s\n", id_nsItem[i][0].text().toLatin1().data(), id_nsItem[i][1].text().toLatin1().data());
                    }
                    break;
                case MAIN_TAB_SMART_LOG:
                    for (i = 0; i < NUM_OF_SMART_ROW; i++)
                    {
                        fprintf(fp, "%s,%s\n", smart_tblItem[i][0].text().toLatin1().data(), smart_tblItem[i][1].text().toLatin1().data());
                    }
                    break;
                case MAIN_TAB_ERROR_LOG:
                    for (i = 0; i < NUM_OF_ERROR_ROW; i++)
                    {
                        fprintf(fp, "%s,%s\n", error_tblItem[i][0].text().toLatin1().data(), error_tblItem[i][1].text().toLatin1().data());
                    }
                    break;
                case MAIN_TAB_FW_LOG:
                    for (i = 0; i < NUM_OF_FW_ROW; i++)
                    {
                        fprintf(fp, "%s,%s\n", fw_tblItem[i][0].text().toLatin1().data(), fw_tblItem[i][1].text().toLatin1().data());
                    }
                    break;
                case MAIN_TAB_REGS:
                    for (i = 0; i < NUM_OF_REGS_ROW; i++)
                    {
                        fprintf(fp, "%s,%s\n", regs_tblItem[i][0].text().toLatin1().data(), regs_tblItem[i][1].text().toLatin1().data());
                    }
                    break;
                default:
                    break;
            }

            fclose(fp);
        }
    }
}

void QNVMe::onMainTabCurrChanged(int index)
{
    switch(index)
    {
        case MAIN_TAB_ID_CTRL:
            Identify(0, CNS_IDFY_CONTROLLER);
            break;
        case MAIN_TAB_ID_NS:
            Identify(dev_info.dev_list[dev_info.curr].nsid, CNS_IDFY_NAMESPACE);
            break;
        case MAIN_TAB_SMART_LOG:
            if (id_ctrl.lpa & 0x01)
            {
                SMARTLog(dev_info.dev_list[dev_info.curr].nsid);
            }
            else
            {
                SMARTLog(0xFFFFFFFF);
            }

            break;
        case MAIN_TAB_ERROR_LOG:
            ErrorLog(dev_info.dev_list[dev_info.curr].nsid, ui->sb_error_index->value());
            break;
        case MAIN_TAB_FW_LOG:
            FWLog(dev_info.dev_list[dev_info.curr].nsid);
            break;
        case MAIN_TAB_FW_UPDATE:
            ui->sb_fw_slot->setMaximum(id_ctrl.frmw);
            break;
        case MAIN_TAB_REGS:
        {
            char buf[128];
            sprintf(buf, "nvme%d", dev_info.dev_list[dev_info.curr].devid);
            ShowRegisters(buf);
            break;
        }
        default:
            break;
    }
}

void QNVMe::onFWCommitClicked(void)
{
    int slot, action;
    struct nvme_passthru_cmd cmd;

    //slot = ui->le_fw_slot->text().toInt();
    slot = ui->sb_fw_slot->value();
    action = ui->le_fw_action->text().toInt();

    // NVMe Identify commannn
    memset(&cmd, 0, sizeof(cmd));
    cmd.opcode = nvme_admin_activate_fw;
    cmd.cdw10 = (action << 3) | slot;

    nvme_ioctl(NVME_IOCTL_ADMIN_CMD, &cmd, NULL, false);
}

void QNVMe::onFWDownloadClicked(void)
{
    int i;
    int bin_size, unit_size;
    FILE* fp;
    char buf[SIZE_1M];
    struct nvme_passthru_cmd cmd;

    fp = fopen(fw_bin_path.toLatin1().data(), "rb");

    if (fp != NULL)
    {
        fseek(fp, 0, SEEK_END);
        bin_size = ftell(fp);
        rewind(fp);

        fread(buf, bin_size, sizeof(char), fp);
        fclose(fp);

        unit_size = ui->le_fw_unit->text().toInt();

        printf ("unit size:%d\n", unit_size);

        for (i = 0; i < bin_size / unit_size; i++)
        {
            // NVMe Identify commannn
            memset(&cmd, 0, sizeof(cmd));
            cmd.opcode = nvme_admin_download_fw;
            cmd.addr  = (unsigned long)&buf[i * unit_size];
            cmd.data_len = unit_size;
            cmd.cdw10 =  (unit_size >> 2) - 1;
            cmd.cdw11 = (unit_size * i) >> 2;
            nvme_ioctl(NVME_IOCTL_ADMIN_CMD, &cmd, (char*)&buf[i * unit_size], true);
        }
    }
    else
    {
        ui->statusBar->showMessage("FW Bin file is not specified!");
    }
}

void QNVMe::onErrorLogIndexChanged(int index)
{
    ErrorLog(0xFFFFFFFF, index);
}

u_int64_t QNVMe::StringToNumber(QString str)
{
    char* p;

    p = str.toLatin1().data() + 1;

    if (p[0] == 'x' || p[0] == 'X')
    {
        return ByteToInteger(p+1, 16);
        //return str.toInt(0, 16);
    }

    return str.toLongLong();
}

void QNVMe::onDSMClicked(void)
{
    struct nvme_passthru_cmd cmd;

    memset(&cmd, 0x0, sizeof(cmd));

    cmd.opcode = nvme_cmd_dsm;
    cmd.nsid   = dev_info.dev_list[dev_info.curr].nsid;
    cmd.cdw10  = ui->sb_dsm_total_range->value() - 1;
    cmd.cdw11  = 1 << ui->cb_dsm_attr->currentIndex();
    cmd.data_len = ui->sb_dsm_total_range->value() * sizeof(struct nvme_dsm_range);
    cmd.addr   = (unsigned long)dsm_range;
    nvme_ioctl(NVME_IOCTL_IO_CMD, &cmd, (char*)dsm_range, true);
}

void QNVMe::onDSMApplyClicked(void)
{
    int index = ui->sb_dsm_curr_range->value();

    dsm_range[index].cattr= StringToNumber(ui->le_dsm_cattr->text());
    dsm_range[index].slba = StringToNumber(ui->le_dsm_lba->text());
    dsm_range[index].nlb  = StringToNumber(ui->le_dsm_len->text());
}

void QNVMe::onDSMResetClicked(void)
{
    memset(dsm_range, 0x0, sizeof(dsm_range));

    ui->le_dsm_cattr->setText("0");
    ui->le_dsm_lba->setText("0");
    ui->le_dsm_len->setText("0");

    ui->sb_dsm_curr_range->setValue(0);
}

void QNVMe::onDSMTotalRangeChanged(int value)
{
    ui->sb_dsm_curr_range->setMaximum(value - 1);
}

void QNVMe::onDSMCurrRangeChanged(int index)
{
    ui->le_dsm_cattr->setText(QString::number(dsm_range[index].cattr));
    ui->le_dsm_lba->setText(QString::number(dsm_range[index].slba));
    ui->le_dsm_len->setText(QString::number(dsm_range[index].nlb));
}

void QNVMe::onDSMFixedClicked(void)
{
    int i, index;
    u_int64_t slba;
    u_int32_t unit;

    slba = StringToNumber(ui->le_dsm_lba->text());
    unit = StringToNumber(ui->le_dsm_unit->text());

    memset(dsm_range, 0x0, sizeof(dsm_range));

    for (i = 0; i < ui->sb_dsm_total_range->value(); i++)
    {
        dsm_range[i].slba = slba + unit * i;
        dsm_range[i].nlb  = unit;
    }

    index = ui->sb_dsm_curr_range->value();
    ui->le_dsm_cattr->setText(QString::number(dsm_range[index].cattr));
    ui->le_dsm_lba->setText(QString::number(dsm_range[index].slba));
    ui->le_dsm_len->setText(QString::number(dsm_range[index].nlb));
}

void QNVMe::onDSMRandomClicked(void)
{
    int i, index;
    int count;
    u_int64_t lba;

    count = qrand() % 256;

    memset(dsm_range, 0x0, sizeof(dsm_range));

    for (i = 0; i < count; i++)
    {
        lba  = qrand() % 0x1000000;
        dsm_range[i].slba = MIN(lba, id_ns.ncap);
        dsm_range[i].nlb  = qrand() % 0x10000;
    }

    ui->sb_dsm_total_range->setValue(count);
    index = ui->sb_dsm_curr_range->value();
    ui->le_dsm_cattr->setText(QString::number(dsm_range[index].cattr));
    ui->le_dsm_lba->setText(QString::number(dsm_range[index].slba));
    ui->le_dsm_len->setText(QString::number(dsm_range[index].nlb));
}

void QNVMe::onDSMLoadClicked(void)
{
    int i = 0;
    FILE* fp;

    dsm_path = QFileDialog::getOpenFileName(this, tr("Open DSM File"),  QDir::currentPath(), tr("DSM (*.dsm)"));

    fp = fopen(dsm_path.toLatin1().data(), "r");

    if (fp != NULL)
    {

        int ret;
        while (true)
        {
            ret = fscanf(fp, "%llx %x %x\n", &dsm_range[i].slba, &dsm_range[i].nlb, &dsm_range[i].cattr);

            if (ret == EOF) break;
            i++;
        }

        fclose(fp);

        ui->sb_dsm_total_range->setValue(i);
    }

    i = ui->sb_dsm_curr_range->value();
    ui->le_dsm_cattr->setText(QString::number(dsm_range[i].cattr));
    ui->le_dsm_lba->setText(QString::number(dsm_range[i].slba));
    ui->le_dsm_len->setText(QString::number(dsm_range[i].nlb));
}

void QNVMe::onDSMSaveClicked(void)
{
    FILE* fp;
    QDateTime date(QDateTime::currentDateTime());
    dsm_path = QFileDialog::getSaveFileName(this, tr("Save DSM File"),  QDir::currentPath()+"/"+date.toString("yyyyMMdd-hhmmss")+".dsm", tr("DSM (*.dsm)"));

    fp = fopen(dsm_path.toLatin1().data(), "w+");

    if (fp != NULL)
    {
        int i;
        for (i = 0; i < ui->sb_dsm_total_range->value(); i++)
        {
            fprintf(fp, "%llx %x %x\n", dsm_range[i].slba, dsm_range[i].nlb, dsm_range[i].cattr);
        }
        fclose(fp);
    }
}

void QNVMe::onBatchBrowseClicked(void)
{
    int i;
    int ret = 0;
    FILE* fp;

    batch_path = QFileDialog::getOpenFileName(this, tr("Open Script File"),  QDir::currentPath(), tr("Script (*.qs)"));
    ui->le_batch->setText(batch_path);

    fp = fopen(batch_path.toLatin1().data(), "r");

    if (fp != NULL)
    {
        i = 0;

        while (1)
        {
            ret = fscanf(fp, "%c %d %d %d\n", &blist[i].op, &blist[i].var[0], &blist[i].var[1], &blist[i].var[2]);
            if (ret == EOF) break;
            i++;
        }

        batch_cnt = i;

        for (i = 0; i < batch_cnt; i++)
        {
            printf("op[%c], var0[%04d] var1[%04d] var2[%04d]\n", blist[i].op, blist[i].var[0], blist[i].var[1], blist[i].var[2]);
        }

        fclose(fp);
    }
}

void QNVMe::onBatchRunClicked(void)
{
    int i;
    struct nvme_user_io io;
    struct nvme_passthru_cmd cmd;

    for (i = 0; i < batch_cnt; i++)
    {
        switch(blist[i].op)
        {
            case 'R':
            case 'r':
                memset(&io, 0x0, sizeof(io));
                io.opcode = nvme_cmd_read;
                io.slba    = blist[i].var[0];
                io.nblocks = blist[i].var[1];
                io.addr    = (unsigned long)tmpBuf;

                nvme_ioctl(NVME_IOCTL_SUBMIT_IO, (struct nvme_passthru_cmd*)&io, tmpBuf, true);
                break;
            case 'W':
            case 'w':
                memset(&io, 0x0, sizeof(io));
                io.opcode = nvme_cmd_write;
                io.slba    = blist[i].var[0];
                io.nblocks = blist[i].var[1];
                io.addr    = (unsigned long)tmpBuf;

                nvme_ioctl(NVME_IOCTL_SUBMIT_IO, (struct nvme_passthru_cmd*)&io, tmpBuf, true);
                break;
            case 'F':
            case 'f':
                memset(&cmd, 0x0, sizeof(cmd));
                cmd.opcode = nvme_cmd_flush;
                cmd.nsid = blist[i].var[0];

                nvme_ioctl(NVME_IOCTL_IO_CMD, &cmd, tmpBuf, false);
                break;
        }
    }
}

void QNVMe::onReadDmesgStdOut(void)
{
    ui->te_dmesg->append(externalProcess.readAllStandardOutput());
    ui->te_dmesg->moveCursor(QTextCursor::End);
}

void QNVMe::onReadDmesgStdErr(void)
{
    ui->te_dmesg->append(externalProcess.readAllStandardError());
    ui->te_dmesg->moveCursor(QTextCursor::End);
}
