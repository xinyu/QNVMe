#ifndef QNVME_H
#define QNVME_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include <QProcess>
#include "nvme.h"

namespace Ui {
class QNVMe;
}

#define BATCH_LIST_NUM      1024
#define BATCH_VAR_NUM       5

#define SIZE_1K             1024
#define SIZE_1M             (1024 * 1024)
#define SIZE_1G             (1024 * 1024 * 1024)

#define U8_MAX              0xFF
#define U16_MAX             0xFFFF
#define U32_MAX             0xFFFFFFFF
#define U64_MAX             0xFFFFFFFFFFFFFFFF

#define S8_MAX              0x7F
#define S16_MAX             0x7FFF
#define S32_MAX             0x7FFFFFFF
#define S64_MAX             0x7FFFFFFFFFFFFFFF

#define MAX_DEV_NUM         1024
#define QNVME_VER           "1.0"

#define MIN(a,b)    a > b ? b : a

enum {
    NUM_OF_ID_CTRL_ROW = 50,
    NUM_OF_ID_NS_ROW   = 30,
    NUM_OF_SMART_ROW   = 20,
    NUM_OF_ERROR_ROW   = 10,
    NUM_OF_FW_ROW      = 10,
    NUM_OF_REGS_ROW    = 12,
};

enum{
    MAIN_TAB_ID_CTRL   = 0,
    MAIN_TAB_ID_NS     = 1,
    MAIN_TAB_SMART_LOG = 2,
    MAIN_TAB_ERROR_LOG = 3,
    MAIN_TAB_FW_LOG    = 4,
    MAIN_TAB_FW_UPDATE = 5,
    MAIN_TAB_ADMIN_CMD = 6,
    MAIN_TAB_NVME_CMD  = 7,
    MAIN_TAB_DSM       = 8,
    MAIN_TAB_REGS      = 9,
};

typedef struct {
    char op;
    int  var[BATCH_VAR_NUM];
}batch_list_t;

typedef struct {
    char* buf;
    int len;
}outbuf_t;

typedef struct {
    int devid;
    int nsid;
}dev_node_t;

typedef struct {
    int cnt;
    int curr;
    dev_node_t dev_list[MAX_DEV_NUM];
}dev_info_t;

class QNVMe : public QMainWindow
{
    Q_OBJECT

public slots:
    void onRefreshClicked();
    void onMountClicked();
    void onUnmountClicked();
    void onDeviceIndexChanged(int );
    void onAdminIndexChanged(int );
    void onNVMeIndexChanged(int );
    void onAdminProceedClicked();
    void onNVMeProceedClicked();
    void onBrowseClicked();
    void onClearClicked();
    void onSaveClicked();
    void onBufFmtGroupClicked();
    void onMainTabCurrChanged(int );
    void onFWDownloadClicked();
    void onFWCommitClicked();
    void onErrorLogIndexChanged(int);
    void onDSMClicked(void);
    void onBatchBrowseClicked();
    void onBatchRunClicked();
    void onDSMTotalRangeChanged(int);
    void onDSMCurrRangeChanged(int);
    void onDSMApplyClicked();
    void onDSMResetClicked();
    void onDSMFixedClicked();
    void onDSMRandomClicked();
    void onDSMLoadClicked();
    void onDSMSaveClicked();

private slots:
    void onReadDmesgStdOut(void);
    void onReadDmesgStdErr(void);

public:
    explicit QNVMe(QWidget *parent = 0);
    ~QNVMe();
    int nvme_ioctl(int cmdcode, struct nvme_passthru_cmd *cmd, char* buf, bool output);
    void ShowDiskInfo(void);
    int GetNsid(int ctrl, int index);
    const char* GetStatusString(unsigned int status);
    void   IntToByte(__u8 *list, char* buf, int len);
    double IntToDouble(__u8* list, int len);
    u_int64_t ByteToInteger(const char* buf, int base);
    u_int64_t StringToNumber(QString str);
    char* FWLogToString(__u64 fw);
private:
    void InitAdminOpTbl(void);
    void InitNVMeOpTbl(void);
    void InitTableWidgets(void);
    int  Identify(int nsid, int cns);
    void ErrorLog(int nsid, int index);
    void SMARTLog(int nsid);
    void FWLog(int nsid);
    void ShowRegisters(char* base);
    void DeviceDiscovery(void);
    void SetupAdminCmdWidgets(int opcode);
    void SetupNVMeCmdWidgets(int opcode);
    void AutoConfigAdminCmd(struct nvme_passthru_cmd* cmd);
    void AutoConfigNVMeCmd(struct nvme_passthru_cmd* cmd);
    void DumpBuffer(char* buf, int len, int append);

    Ui::QNVMe *ui;
    int fd;

    char admin_op_tbl[19];
    char nvme_op_tbl[12];

    QString fw_bin_path;
    QString output_path;
    QString batch_path;
    QString dsm_path;

    outbuf_t buflog;
    dev_info_t dev_info;
    batch_list_t blist[BATCH_LIST_NUM];
    int batch_cnt;

    struct nvme_id_ctrl id_ctrl;
    struct nvme_id_ns   id_ns;
    int namesapce_list[1024];
    short controller_list[2048];
    struct nvme_smart_log smart_log;
    struct nvme_error_log_page error_log[64];
    struct nvme_firmware_log_page fw_log;
    struct nvme_bar nvme_bar_info;
    struct nvme_dsm_range dsm_range[256];
    int dsm_curr_range;

    QTableWidgetItem id_tblItem[NUM_OF_ID_CTRL_ROW][2];
    QTableWidgetItem id_nsItem[NUM_OF_ID_NS_ROW][2];
    QTableWidgetItem smart_tblItem[NUM_OF_SMART_ROW][2];
    QTableWidgetItem error_tblItem[NUM_OF_ERROR_ROW][2];
    QTableWidgetItem fw_tblItem[NUM_OF_FW_ROW][2];
    QTableWidgetItem regs_tblItem[NUM_OF_REGS_ROW][2];

    QProcess externalProcess;

    char* tmpBuf;
    char* tmpRvsBuf;
};

#endif // QNVME_H
